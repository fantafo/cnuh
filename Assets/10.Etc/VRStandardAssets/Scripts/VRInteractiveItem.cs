using System;
using UnityEngine;

namespace VRStandardAssets.Utils
{
    // This class should be added to any gameobject in the scene
    // that should react to input based on the user's gaze.
    // It contains events that can be subscribed to by classes that
    // need to know about input specifics to this gameobject.
    // 이 클래스는 모든 게임 객체에 추가되어야 함.
    // 사용자의 시선 혹은 입력에 반응해야함.
    // 클래스가 참조 할 수있는 이벤트를 포함.
    // 소속 게임 객체에 대한 입력(VRInput) 세부 사항을 알아야함.
    public class VRInteractiveItem : MonoBehaviour
    {
        public event Action OnOver;             // 바라본다! Called when the gaze moves over this object
        public event Action OnOut;              // 바라봤다! Called when the gaze leaves this object
        public event Action OnClick;            // 클릭! Called when click input is detected whilst the gaze is over this object.
        public event Action OnDoubleClick;      // 더블클릭! Called when double click input is detected whilst the gaze is over this object.
        public event Action OnUp;               // 누르고 난 후 순간! Called when Fire1 is released whilst the gaze is over this object.
        public event Action OnDown;             // 누를 때 순간! Called when Fire1 is pressed whilst the gaze is over this object.


        protected bool m_IsOver; // 현재 바라보는지?

        // public 필드
        public bool IsOver
        {
            get { return m_IsOver; }              // Is the gaze currently over this object?
        }


        // The below functions are called by the VREyeRaycaster when the appropriate input is detected.
        // They in turn call the appropriate events should they have subscribers.
        // 아래 함수는 적절한 입력이 감지 될 때 VREyeRaycaster에 의해 호출.
        // 참조자가 있으면 적절한 이벤트를 차례로 호출함.
        public void Over()
        {
            m_IsOver = true;

            if (OnOver != null)
                OnOver();
        }

        public void Out()
        {
            m_IsOver = false;

            if (OnOut != null)
                OnOut();
        }

        public void Click()
        {
            if (OnClick != null)
                OnClick();
        }

        public void DoubleClick()
        {
            if (OnDoubleClick != null)
                OnDoubleClick();
        }

        public void Up()
        {
            if (OnUp != null)
                OnUp();
        }

        public void Down()
        {
            if (OnDown != null)
                OnDown();
        }
    }
}