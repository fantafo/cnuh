﻿using FTF.CommandAuto;
using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// CommandReceiver가 사용하기 어려운 경우 Unity의 SendMessage와 비슷한 용도로 사용할 수 있게 제공하는 클래스다.
    /// [OnCommand]를 사용한 메서드의 경우에만 SendMessage같이 사용할 수 있게된다.
    /// 또한 OnCommandListener로 지정된 메서드의 변수는 primity 타입 혹은 Vector, Quaternion, Rect, Bounds만 사용할 수 있다.
    /// </summary>
    public class IdentityCommander : AutoCommander
    {
        public static bool AutoIdentity = true;
        public uint TargetId { get { return impl.TargetId; } set { impl.TargetId = value; } }

        protected override void Awake()
        {
            base.Awake();
            if (AutoIdentity)
                RegistIdentity();
        }

        public void RegistIdentity()
        {
            IEnumerator e = registIdentityCoroutine();
            if(e.MoveNext())
                StartCoroutine(e);
        }
        IEnumerator registIdentityCoroutine()
        {
            while (Networker.State < NetworkState.Room)
                yield return null;

            Networker.Send(C_Scene.RequestCommanderID(transform.Path(), GetType().FullName));
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public override void Broadcast(string methodName, params object[] param)
        {
            base.Broadcast(methodName, param);
        }

        public override void Send(PlayerInstance player, string methodName, params object[] param)
        {
            base.Send(player, methodName, param);
        }
    }
}
