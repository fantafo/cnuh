﻿using System;
using UnityEngine;
using System.Collections;
using Pvr_UnitySDKAPI;
using FTF;

/*Pvr_ControllerDemo를 복사해서 작성
 * 컨트롤러를 사용한다면 실행됩니다.
 * 아이커서 혹은 headsetcontroller일 경우 
 * FTFReticlePvrPointer를 사용합니다.
 */
public class FTFPvr_Controller : MonoBehaviour
{
    public GameObject HeadSetController;
    public GameObject controller0;


    public Transform direction;

    private Ray ray;
    private GameObject currentController;

    private Transform lastHit;
    private Transform currentHit;

    private bool noClick;
    private float dotSize = 0.5f;
    private float defaultDotScale;

    // Use this for initialization
    void Start()
    {
        ray = new Ray();
        Pvr_ControllerManager.PvrServiceStartSuccessEvent += ServiceStartSuccess;
        Pvr_ControllerManager.SetControllerStateChangedEvent += ControllerStateListener;
        Pvr_ControllerManager.ControllerStatusChangeEvent += CheckControllerStateForGoblin;
#if UNITY_EDITOR
        currentController = controller0;
#endif
        if (Pvr_UnitySDKManager.SDK != null)
        {
            if (Pvr_UnitySDKManager.pvr_UnitySDKSensor != null)
            {
                Pvr_UnitySDKManager.pvr_UnitySDKSensor.ResetUnitySDKSensor();
            }
            else
            {
                Pvr_UnitySDKManager.SDK.pvr_UnitySDKEditor.ResetUnitySDKSensor();
            }

        }
        //ray.origin = transform.position;
        defaultDotScale = Vector3.Distance(GameObject.Find("LeftEye").transform.position, GameObject.Find("customDot").transform.position);
        defaultDotScale = defaultDotScale * dotSize;
    }

    void OnDestroy()
    {
        Pvr_ControllerManager.PvrServiceStartSuccessEvent -= ServiceStartSuccess;
        Pvr_ControllerManager.SetControllerStateChangedEvent -= ControllerStateListener;
        Pvr_ControllerManager.ControllerStatusChangeEvent -= CheckControllerStateForGoblin;
    }

    // Update is called once per frame
    void Update()
    {
        controller0.SetActive(true);


        if (!HeadSetController.activeSelf)
        {

            if (currentController != null)
            {
                //ray.direction = direction.position - transform.position;
                ray.direction = currentController.transform.Find("dot").position - currentController.transform.Find("start").position;
                ray.origin = currentController.transform.Find("start").position;
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {

                    currentHit = hit.transform;

                    if (currentHit != null && lastHit != null && currentHit != lastHit)
                    {
                        if (lastHit.GetComponent<Pvr_UIGraphicRaycaster>() && lastHit.transform.gameObject.activeInHierarchy && lastHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled)
                        {
                            lastHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled = false;
                        }
                        //Debug.Log("hit object : " + currentHit.transform.gameObject.name);
                    }
                    if (currentHit != null && lastHit != null && currentHit == lastHit)
                    {
                        if (currentHit.GetComponent<Pvr_UIGraphicRaycaster>() && !currentHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled)
                        {
                            currentHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled = true;

                        }
                    }
                    lastHit = hit.transform;
                    Debug.DrawLine(ray.origin, hit.point, Color.red);

                    currentController.transform.Find("dot").position = hit.point;
                    float distance = Vector3.Distance(GameObject.Find("LeftEye").transform.position, hit.point);
                    GameObject.Find("dot").transform.localScale = new Vector3(dotSize * distance, dotSize * distance, 178.7027f);
                    gameObject.GetComponent<LineRenderer>().SetPosition(0, ray.origin);
                    gameObject.GetComponent<LineRenderer>().SetPosition(1, hit.point);

                }
                else
                {
                    GameObject.Find("dot").transform.localScale = new Vector3(defaultDotScale, defaultDotScale, 178.7027f);

                    currentController.transform.Find("dot").position = currentController.transform.Find("customDot").position;
                    currentController.transform.Find("customDot").gameObject.SetActive(true);
                    currentHit = null;
                    noClick = false;
                    gameObject.GetComponent<LineRenderer>().SetPosition(0, ray.origin);
                    gameObject.GetComponent<LineRenderer>().SetPosition(1, GameObject.Find("dot").transform.position);
                }


            }

            if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.TOUCHPAD) ||
                Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.TOUCHPAD) ||
                Input.GetKeyDown(KeyCode.JoystickButton0) || Input.GetMouseButtonDown(0))
            {
                if (lastHit != null && 1 << lastHit.transform.gameObject.layer == LayerMask.GetMask("Coin") && currentHit != null)
                {
                    var target = currentHit.gameObject;
                    if (target != null)
                    {
                        //var coinPoint = target.GetComponent<HiddenFinderItem>();
                        //if (coinPoint != null)
                        //{
                        //    coinPoint.OnClick();
                        //}

                        //var coinPoint2 = target.GetComponent<CoinChanceItem>();
                        //if (coinPoint2 != null)
                        //{
                        //    coinPoint2.OnClick();
                        //}
                    }
                    noClick = true;
                }
            }
        }
        else
        {

        }
    }




    private void ServiceStartSuccess()
    {
        if (Controller.UPvr_GetControllerState(0) == ControllerState.Connected ||
          Controller.UPvr_GetControllerState(1) == ControllerState.Connected)
        {
            HeadSetController.SetActive(false);
        }
        else
        {
            HeadSetController.SetActive(true);
        }
        if (Controller.UPvr_GetMainHandNess() == 0)
        {
            currentController = controller0;
        }
    }

    private void ControllerStateListener(string data)
    {

        if (Controller.UPvr_GetControllerState(0) == ControllerState.Connected ||
                  Controller.UPvr_GetControllerState(1) == ControllerState.Connected)
        {
            HeadSetController.SetActive(false);
        }
        else
        {
            HeadSetController.SetActive(true);
        }

        if (Controller.UPvr_GetMainHandNess() == 0)
        {
            currentController = controller0;
        }
    }

    private void CheckControllerStateForGoblin(string state)
    {
        HeadSetController.SetActive(!Convert.ToBoolean(Convert.ToInt16(state)));
    }

}
