﻿using UnityEngine;
using UnityEngine.VR;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using System.IO;
using System.Diagnostics;

/// <summary>
/// Author : 정상철
/// </summary>
namespace SFramework.Editors
{
    [InitializeOnLoad]
    public class PlatformManagerTools
    {
        static string Identifier
        {
            get
            {
#if UNITY_5_6_OR_NEWER
                return PlayerSettings.applicationIdentifier;
#else
                return PlayerSettings.bundleIdentifier;
#endif
            }
        }

        [MenuItem("SF/Platform/Android/Uninstall")]
        public static void AndroidUninstallPackage()
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = @"adb.exe",
                Arguments = "uninstall " + Identifier,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };
            Process p = new Process { StartInfo = psi };
            if (p.Start())
            {
                EditorUtility.DisplayDialog("명령어 실행", "제거성공", "확인");
            }
        }
        [MenuItem("SF/Platform/Android/Start")]
        public static void AndroidStart()
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = @"adb.exe",
                Arguments = "shell monkey -p " + Identifier + " -c android.intent.category.LAUNCHER 1",
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };
            Process p = new Process { StartInfo = psi };
            if (p.Start())
            {
                EditorUtility.DisplayDialog("명령어 실행", "실행 성공", "확인");
            }
        }
        [MenuItem("SF/Platform/Android/Stop")]
        public static void AndroidStop()
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = @"adb.exe",
                Arguments = "shell am force-stop " + Identifier,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };
            Process p = new Process { StartInfo = psi };
            if (p.Start())
            {
                EditorUtility.DisplayDialog("명령어 실행", "종료 성공", "확인");
            }
        }
    }
}