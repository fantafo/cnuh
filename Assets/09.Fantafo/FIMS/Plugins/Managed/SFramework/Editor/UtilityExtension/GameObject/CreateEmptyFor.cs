﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class CreateEmptyFor : BaseExtensionEditor
    {
        [MenuItem("GameObject/Create Empty/On Selected Objects %&#n")]
        public static void CreateEmptyOnSelectedObject()
        {
            if (init("Create Empty On Selected Objects", 0))
            {
                var parent = transforms[0].parent;
                var sibiling = transforms[0].GetSiblingIndex();

                var go = new GameObject(transforms[0].name + " parent");

                // transform 정보 복사
                go.transform.SetParent(transforms[0], false);
                go.transform.SetParent(parent, true);

                go.transform.SetSiblingIndex(sibiling);

                Loop(t =>
                {
                    t.SetParent(go.transform);
                });

                Selection.activeObject = go;
            }
        }

    }
}