﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class TransformRecovery : EditorWindow
    {
        [MenuItem("GameObject/Transform/Recovery Transforms")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<TransformRecovery>("Transform Recovery");
        }

        [MenuItem("GameObject/Transform/Recovery T-Pose")]
        public static void RecoveryTPose()
        {
            Transform root = Selection.activeTransform;
            if (root == null)
            { EditorUtility.DisplayDialog("Recovery T-Pose", "Animator가 속해있거나, 자식인 Transform을 선택해주세요.", "확인"); return; }

            Animator anim = root.GetComponentInParent<Animator>();
            if (anim == null)
            { EditorUtility.DisplayDialog("Recovery T-Pose", "Animator가 속해있거나, 자식인 Transform을 선택해주세요.", "확인"); return; }

            Avatar avatar = anim.avatar;
            if (avatar == null)
            { EditorUtility.DisplayDialog("Recovery T-Pose", "Avatar가 포함 돼 있는 휴머노이드여야합니다.", "확인"); return; }

            string path = AssetDatabase.GetAssetPath(avatar);
            if (string.IsNullOrEmpty(path))
            { EditorUtility.DisplayDialog("Recovery T-Pose", "아바타의 원형이 존재하지 않습니다.", "확인"); return; };

            GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            if (go == null)
            { EditorUtility.DisplayDialog("Recovery T-Pose", "아바타의 원형이 존재하지 않습니다.", "확인"); return; };

            fromTransform = go.transform;
            toTransform = anim.transform;

            Recovery();
        }

        void Start()
        {
            fromTransform = null;
            toTransform = null;
        }

        void OnGUI()
        {
            EditorGUILayout.HelpBox("From의 자식 Transform정보를 To의 자식의 Transform에게 입력합니다.", MessageType.Info);

            fromTransform = EditorGUILayout.ObjectField("From", fromTransform, typeof(Transform), true) as Transform;
            toTransform = EditorGUILayout.ObjectField("To", toTransform, typeof(Transform), true) as Transform;

            if (GUILayout.Button("Recovery") && fromTransform && toTransform)
            {
                Recovery();
            }
        }


        public static Transform fromTransform;
        public static Transform toTransform;

        public static void Recovery()
        {
            Dictionary<string, Transform> fromObjs = GetTransformChildList(fromTransform);
            Dictionary<string, Transform> toObjs = GetTransformChildList(toTransform);

            foreach (var pair in toObjs)
            {
                Transform to = pair.Value;
                Transform from;
                if (fromObjs.TryGetValue(pair.Key, out from))
                {
                    to.localPosition = from.localPosition;
                    to.localRotation = from.localRotation;
                    to.localScale = from.localScale;
                }
            }
        }

        private static Dictionary<string, Transform> GetTransformChildList(Transform trans)
        {
            Dictionary<string, Transform> result = new Dictionary<string, Transform>();
            SearchChild(trans, "", result);
            return result;
        }
        private static void SearchChild(Transform parent, string path, Dictionary<string, Transform> result)
        {
            for (int i = 0; i < parent.childCount; i++)
            {
                var child = parent.GetChild(i);
                var name = path + "." + child.name;
                result.Add(name, child);
                SearchChild(child, name, result);
            }
        }
    }
}