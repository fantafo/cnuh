﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.SceneManagement;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class HideFlagClear : BaseExtensionEditor
    {
        [MenuItem("GameObject/Clear/HideFlags")]
        public static void ClearHideFlag()
        {
            Debug.Log("--------------------------------");
            List<GameObject> gos = new List<GameObject>();
            foreach (var tag in UnityEditorInternal.InternalEditorUtility.tags)
            {
                foreach (var go in GameObject.FindGameObjectsWithTag(tag))
                {
                    if (go.hideFlags != HideFlags.None)
                    {
                        go.hideFlags = HideFlags.None;
                        Debug.Log(go.name, go);
                        gos.Add(go);
                    }
                }
            }
            foreach (var go in GameObject.FindObjectsOfType<GameObject>())
            {
                if (go.hideFlags != HideFlags.None)
                {
                    go.hideFlags = HideFlags.None;
                    Debug.Log(go.name, go);
                    gos.Add(go);
                }
            }
            Selection.objects = gos.ToArray();
            Debug.Log("--------------------------------");
        }

        [MenuItem("GameObject/Clear/All")]
        public static void ClearAll()
        {
            foreach (var tag in UnityEditorInternal.InternalEditorUtility.tags)
            {
                foreach (var go in GameObject.FindGameObjectsWithTag(tag))
                {
                    GameObject.DestroyImmediate(go);
                }
            }
            foreach (var go in GameObject.FindObjectsOfType<GameObject>())
            {
                GameObject.DestroyImmediate(go);
            }
        }


    }
}