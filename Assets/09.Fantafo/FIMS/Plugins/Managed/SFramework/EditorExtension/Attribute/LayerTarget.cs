﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UObject = UnityEngine.Object;
using URandom = UnityEngine.Random;

[Serializable]
public struct LayerTarget
{
    public int value;

    public static implicit operator int(LayerTarget mask)
    {
        return mask.value;
    }
    public static implicit operator LayerTarget(int intVal)
    {
        return new LayerTarget { value = intVal };
    }
}
