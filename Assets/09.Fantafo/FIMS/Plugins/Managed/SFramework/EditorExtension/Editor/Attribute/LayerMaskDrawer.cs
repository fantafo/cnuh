﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UObject = UnityEngine.Object;
using URandom = UnityEngine.Random;

namespace Sions.Drawer
{
    [CustomPropertyDrawer(typeof(LayerTarget))]
    public class LayerMaskDrawer : PropertyDrawer
    {
        static GUIContent[] layers = null;
        static double loadedTime;
        static int[] layerValues = null;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (layers == null || loadedTime + 10 < EditorApplication.timeSinceStartup)
            {
                loadedTime = EditorApplication.timeSinceStartup;

                List<GUIContent> layerList = new List<GUIContent>();
                List<int> layerValueList = new List<int>();
                for (int i = 0; i < 32; i++)
                {
                    string name = LayerMask.LayerToName(i);
                    if (!string.IsNullOrEmpty(name))
                    {
                        layerList.Add(new GUIContent(name));
                        layerValueList.Add(i);
                    }
                }
                layers = layerList.ToArray();
                layerValues = layerValueList.ToArray();
            }

            SerializedProperty value = property.FindPropertyRelative("value");

            int val = value.intValue;
            int newVal = EditorGUI.IntPopup(position, label, val, layers, layerValues);
            if (val != newVal)
            {
                value.intValue = newVal;
                value.serializedObject.ApplyModifiedProperties();
            }
        }
    }
}