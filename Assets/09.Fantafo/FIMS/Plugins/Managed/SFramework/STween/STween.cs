﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using SFramework;
using SFramework.TweenAction;
using System.Reflection;
using System.Threading;

public partial class STween : MonoBehaviour
{
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Excpeitons                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public class STweenException : ApplicationException
    {
        public STweenException(string message) : base(message)
        { }
    }




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Variables                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    // CurrentRunning Tween state
    private static ObjectLinkedList<STweenState> s_tweens;

    // tween state pool 
    private static STweenStatePool s_pool;

    // action pools
    private static Dictionary<Type, STweenActionPool> s_actions;

    // Stween is Running?
    private static bool s_updating;

    // scaled time. set on LifeCycle update
    private static float s_time;
    // unscaled time. set on LifeCycle update
    private static float s_unscaledTime;

    // initialize STweenState Pool Size
    private static int s_stateInitializeCount;
    // increase STweenState Pool Size
    private static int s_stateIncreaseCount;

    // initialize Each Tween Action Pool Size
    private static int s_actionInitializeCount;

    // increase Each Tween Action Pool Size.
    private static int s_actionIncreaseCount;

    // current Select State
    private static STweenState s_currentState;

    // object to verify that the available StweenState.
    // [Kor] STweenState가 유효한지 검증하기위한 객체.
    internal static object CONTAINER_SIGNITURE = new object();



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Properties                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    /// <summary>
    /// Count of play for tween
    /// </summary>
    public static int PlayCount
    {
        get
        {
            return s_tweens.Count;
        }
    }


    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                          Initialize                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

#if !DISABLE_STWEEN_INIT
    static STween()
    {
        Initialize();
    }
#endif

    /// <summary>
    /// 
    /// initialize STween. First Tween Action before called this. 
    /// 
    /// </summary>
    /// <param name="stateCap">set to s_stateInitializeCount.</param>
    /// <param name="stateIncreaseCount">set to s_stateIncreaseCount.</param>
    /// <param name="actionCap">set to s_actionInitializeCount.</param>
    /// <param name="actionIncreaseCount">set to s_actionIncreaseCount.</param>
    /// <param name="actionListCap">initialize action Dictionary capacity.</param>
    public static void Initialize(int stateCap = 1000, int stateIncreaseCount = 1000, int actionCap = 10, int actionIncreaseCount = 10, int actionListCap = 200)
    {
        if (s_tweens != null)
            return;

        // Create GameObject and Component
        STween tweenObj;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
        tweenObj = GameObject.FindObjectOfType(typeof(STween)) as STween;
#else
        tweenObj = GameObject.FindObjectOfType<STween>();
#endif
        if (!tweenObj)
        {
            GameObject go = new GameObject("~STween");
            tweenObj = go.AddComponent<STween>();
        }
        // Enable To Tween System
        tweenObj.transform.parent = SF.Hidden;
        if (!tweenObj.enabled)
            tweenObj.enabled = true;
        if (!tweenObj.gameObject.activeSelf)
            tweenObj.gameObject.SetActive(true);

        // setup pool size
        s_stateInitializeCount = stateCap;
        s_stateIncreaseCount = stateIncreaseCount;
        s_actionInitializeCount = actionCap;
        s_actionIncreaseCount = actionIncreaseCount;

        // create running state List
        s_tweens = new ObjectLinkedList<STweenState>();

        // create action pool Dictionary
        s_actions = new Dictionary<Type, STweenActionPool>(actionListCap);

        // create STweenState Pool
        s_pool = new STweenStatePool()
        {
            initializeCount = s_stateInitializeCount,
            increaseCount = s_stateIncreaseCount
        };
        s_pool.Prepare();
    }

    public static void InitializeActions()
    {
        if (s_actions == null)
            s_actions = new Dictionary<Type, STweenActionPool>();

        Type actionType = typeof(BaseTweenAction);
        STweenActionPool pool;
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (!type.IsAbstract && type.IsSubclassOf(actionType))
                {
                    pool = s_actions.Get(type);
                    if (pool == null)
                    {
                        pool = new STweenActionPool(type, s_actionInitializeCount, s_actionIncreaseCount);
                        pool.Prepare();
                        s_actions.Add(type, pool);
                    }
                    Debug.Log(type.FullName);
                }
            }
        }
    }
    public static void InitializeAction<T>(int count)
    {
        InitializeAction(typeof(T), count);
    }
    public static void InitializeAction(Type type, int count)
    {
        var pool = s_actions.Get(type);
        if (pool == null)
        {
            pool = new STweenActionPool(type, count, s_actionIncreaseCount);
            pool.Prepare();
            s_actions.Add(type, pool);
        }
        else
        {
            pool.Push(count);
        }
    }


    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                    Tween Pools Extension                    //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    /// <summary>
    /// Get STweenState From StatePool.
    /// </summary>
    /// <param name="duration">set default Duration</param>
    /// <returns></returns>
    public static STweenState NewState(float duration)
    {
        Initialize();

#if UNITY_EDITOR || UNITY_ANDROID
        RECOLLECT:

        var temp = s_pool._enableClear;
        s_pool._enableClear = false;
        var state = s_pool.Take();
        while (state == s_currentState)
            state = s_pool.Take();
        s_pool._enableClear = temp;

        if (s_tweens.Contains(state))
        {
            Debug.LogError("Duplication State " + state.ToString());
            goto RECOLLECT;
        }
        state.Clear();
#else
        var state = s_pool.Take();
#endif
        state.Duration(duration);
        state.AddPlayId();

#if STWEEN_DEBUG
        Debug.LogWarning("ADD " + state.instanceID);
#endif

#if UNITY_EDITOR
        try
        { throw new System.Exception("Start Point"); }
        catch (System.Exception e) { state.callStack = e.StackTrace.ToString(); }
#endif

        return state;
    }
    /// <summary>
    /// Release STweenState To StatePool.
    /// </summary>
    /// <param name="state"></param>
    /// <returns></returns>
    public static void ReleaseState(STweenState state)
    {
        s_pool.Release(state);
    }

    /// <summary>
    /// Get Tween Action From ActionPool Dictionary. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T NewAction<T>()
        where T : BaseTweenAction
    {
        Initialize();
        Type type = typeof(T);
        STweenActionPool pool = s_actions.Get(type);
        if (pool == null)
        {
            pool = new STweenActionPool(type, s_actionInitializeCount, s_actionIncreaseCount);
            s_actions.Add(type, pool);
        }

#if STWEEN_DEBUG
        Debug.Log("STween : New Action");
#endif
        return (T)pool.Take();
    }


    /// <summary>
    /// Release Action To ActionPool Dictionary.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="action"></param>
    public static void ReleaseAction(BaseTweenAction action)
    {
        if (action != null)
        {
            ReleaseAction(action._type, action);
        }
    }
    public static void ReleaseAction(Type type, BaseTweenAction action)
    {
        STweenActionPool pool;
        if (s_actions.TryGetValue(type, out pool))
        {
            pool.Release(action);
        }
    }


    /// <summary>
    /// Attach To Running List
    /// </summary>
    /// <param name="state"></param>
    public static void Attach(STweenState state)
    {
        Initialize();
        s_tweens.Add(state);
    }
    /// <summary>
    /// Pause state
    /// </summary>
    /// <param name="state"></param>
    public static void Pause(STweenState state)
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
            state._pauseTime = (float)UnityEditor.EditorApplication.timeSinceStartup;
        else
#endif
            state._pauseTime = state._unscaledTime ? STime.unscaledTime : Time.time;
        state._pause = true;
    }
    /// <summary>
    /// Resum state
    /// </summary>
    /// <param name="state"></param>
    public static void Resume(STweenState state)
    {
        if (state._pause)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                state._startTime += (float)(UnityEditor.EditorApplication.timeSinceStartup - state._pauseTime);
            }
            else
#endif
            {
                state._startTime += ((state._unscaledTime ? STime.unscaledTime : Time.time) - state._pauseTime);
            }
            state._pause = false;
        }
    }
    /// <summary>
    /// Replay state
    /// </summary>
    /// <param name="state"></param>
    public static void Replay(STweenState state)
    {
        state.Replay();
    }
    /// <summary>
    /// stop state and clear datas. Called After Can't recycling
    /// </summary>
    /// <param name="state"></param>
    public static void Stop(STweenState state, bool callOnComplete = false)
    {
        if (s_tweens == null)
            return;
        s_tweens.Remove(state);
        _clear(state);
        if (callOnComplete)
            state.ExecuteOnComplete();
        else
            state.ExecuteOnCancel();
    }
    private static void _clear(STweenState state)
    {
        state.AddPlayId();
        if ((state._releasable & STweenReleasable.State) != 0)
        {
            ReleaseState(state);
        }
        if ((state._releasable & STweenReleasable.Action) != 0)
        {
            ReleaseAction(state._action);
        }
    }

    /// <summary>
    /// Stop All Tweens
    /// </summary>
    public static void StopAll(bool callOnComplete = false)
    {
        STweenState state;
        if (callOnComplete)
        {
            state = s_tweens.First;
            while (state != s_tweens.NULL)
            {
                state.ExecuteOnComplete();
                state = state.Next;
            }
        }
        else
        {
            state = s_tweens.First;
            while (state != s_tweens.NULL)
            {
                state.ExecuteOnCancel();
                state = state.Next;
            }
        }

        state = s_tweens.First;
        while (state != s_tweens.NULL)
        {
            _clear(state);
            state = state.Next;
        }
        s_tweens.Clear();

#if STWEEN_DEBUG
        Debug.Log("STween : Stop All");
#endif
    }
    public static void StopAll(GameObject gameObject, bool callOnComplete)
    {
        foreach (var target in Find(gameObject))
        {
            Stop(target, callOnComplete);
        }
    }
    public static void StopAll(string tag, bool callOnComplete)
    {
        var list = ListPool<STweenState>.Take();
        try
        {
            FindTag(tag, list);
            for (int i = 0; i < list.Count; i++)
            {
                Stop(list[i], callOnComplete);
            }
        }
        finally
        {
            list.Release();
        }
    }
    public static void StopAll(object r, bool callOnComplete)
    {
        var list = ListPool<STweenState>.Take();
        try
        {
            FindRef(r, list);
            for (int i = 0; i < list.Count; i++)
            {
                Stop(list[i], callOnComplete);
            }
        }
        finally
        {
            list.Release();
        }
    }




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                        Get Current Tweens                   //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    /// <summary>
    /// Get All STweenState for the GameObject
    /// </summary>
    /// <param name="go"></param>
    /// <returns></returns>
    public static STweenState[] Find(GameObject go)
    {
        var list = ListPool<STweenState>.Take();
        try
        {
            Find(go, list);
            return list.ToArray();
        }
        finally
        {
            ListPool<STweenState>.Release(list);
        }
    }
    public static void Find(GameObject go, List<STweenState> list)
    {
        STweenState state = s_tweens.First;
        if (state != null)
        {
            do
            {
                if (state._checkGameObject == go)
                {
                    list.Add(state);
                }
                else if (state._checkBehaviour != null && state._checkBehaviour.gameObject == go)
                {
                    list.Add(state);
                }
            }
            while ((state = state.Next) != s_tweens.NULL);
        }
    }

    /// <summary>
    /// Get All STweenState for the GameObject
    /// </summary>
    /// <param name="go"></param>
    /// <returns></returns>
    public static STweenState[] Find<T>()
        where T : BaseTweenAction
    {
        var list = ListPool<STweenState>.Take();
        try
        {
            Find<T>(list);
            return list.ToArray();
        }
        finally
        {
            ListPool<STweenState>.Release(list);
        }
    }
    public static void Find<T>(List<STweenState> list)
        where T : BaseTweenAction
    {
        STweenState state = s_tweens.First;
        if (state != null)
        {
            do
            {
                if (state._action is T)
                {
                    list.Add(state);
                }
            }
            while ((state = state.Next) != s_tweens.NULL);
        }
    }

    /// <summary>
    /// Get All STweenState for the Tag
    /// </summary>
    /// <param name="tag"></param>
    /// <returns></returns>
    public static STweenState[] FindTag(string tag)
    {
        var list = ListPool<STweenState>.Take();
        try
        {
            FindTag(tag, list);
            return list.ToArray();
        }
        finally
        {
            ListPool<STweenState>.Release(list);
        }
    }
    public static void FindTag(string tag, List<STweenState> list)
    {
        STweenState state = s_tweens.First;
        if (state != null)
        {
            do
            {
                if (state.Tag == tag)
                {
                    list.Add(state);
                }
            }
            while ((state = state.Next) != s_tweens.NULL);
        }
    }

    /// <summary>
    /// Get All STweenState for the Tag
    /// </summary>
    /// <param name="tag"></param>
    /// <returns></returns>
    public static STweenState[] FindRef(object r)
    {
        var list = ListPool<STweenState>.Take();
        try
        {
            FindRef(r, list);
            return list.ToArray();
        }
        finally
        {
            ListPool<STweenState>.Release(list);
        }
    }
    public static void FindRef(object r, List<STweenState> list)
    {
        STweenState state = s_tweens.First;
        if (state != null)
        {
            do
            {
                if (state.Ref == r)
                {
                    list.Add(state);
                }
            }
            while ((state = state.Next) != s_tweens.NULL);
        }
    }

    /// <summary>
    /// Get All STweenState for the GameObject
    /// </summary>
    /// <param name="go"></param>
    /// <returns></returns>
    public static STweenState[] GetStateAll()
    {
        STweenState[] array = new STweenState[s_tweens.Count];
        int index = 0;

        STweenState state = s_tweens.First;
        if (state != null)
        {
            do
            {
                array[index++] = state;
            }
            while ((state = state.Next) != s_tweens.NULL);
        }
        return array;
    }
    public static void GetStateAll(List<STweenState> list)
    {
        list.Clear();

        STweenState state = s_tweens.First;
        if (state != null)
        {
            do
            {
                if (!list.Contains(state))
                    list.Add(state);
            }
            while ((state = state.Next) != s_tweens.NULL);
        }
    }




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Life Cycles                       //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    /// <summary>
    /// Update Tween
    /// </summary>
    private void Update()
    {
        UpdateAll(false);
    }

    /// <summary>
    /// Late Update Tween
    /// </summary>
    private void LateUpdate()
    {
        UpdateAll(true);
    }


#if UNITY_EDITOR
    /// <summary>
    /// Validate After Compile
    /// </summary>
    public void OnValidate()
    {
        if (Application.isPlaying && s_tweens == null)
        {
            Initialize();
        }
    }
#endif

    /// <summary>
    /// Update Tween
    /// </summary>
    /// <param name="lateUpdate">If the caller is LateUpdate Cycle</param>
    public static void UpdateAll(bool lateUpdate)
    {
        s_updating = true;
        s_time = Time.time;
        s_unscaledTime = STime.unscaledTime;

        STweenState state = s_tweens.First, next;
        while (state != null)
        {
            next = state.Next;
            {
                if (state._lateUpdate == lateUpdate)
                {
                    s_currentState = state;
                    try
                    {
                        UpdateTween(state);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                        Stop(state, false);
                    }
                    s_currentState = null;
                }
            }
            state = next;
        }

        s_updating = false;
    }

    /// <summary>
    /// Update Single Tween State
    /// </summary>
    /// <param name="tween"></param>
    /// <returns>'True' is Have Remove Tween.</returns>
    public static bool UpdateTween(STweenState tween)
    {
        if (tween._pause)
            return false;

        // initialize
        if (s_updating == false)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                s_time = s_unscaledTime = (float)UnityEditor.EditorApplication.timeSinceStartup;
            }
            else
#endif
            {
                s_time = Time.time;
                s_unscaledTime = STime.unscaledTime;
            }
        }


        // select time table
        float targetTime;
        if (tween._unscaledTime)
            targetTime = s_unscaledTime;
        else
            targetTime = s_time;

        // initialize tween state
        if (!tween._initialized)
        {
            tween.Prepare(targetTime);
        }

        // delay skip
        //if (targetTime < tween._startTime)
        //    return false;

        if (!tween._started)
            tween.ExecuteOnStart();

        // check enable
        if ((tween._checkBehaviour != null && !tween.EnableBehaviour)
            || (tween._checkGameObject != null && !tween.EnableGameObject))
        {
            return false;
        }

        float ratio = 1;
        bool tweenEnd = true;
        if (tween._duration > 0)
        {
            // calculate ratio
            ratio = Mathf.Clamp01((targetTime - tween._startTime) / tween._duration);
            tweenEnd = (ratio == 1);

            // direction
            if (!tween._forward && !tween._easeReverse)
                ratio = 1 - ratio;

            // easing
            if (tween._easeFunc != null)
                ratio = tween._easeFunc(ratio);
            else if (tween._easeType == SEasingType.animationCurve && tween._easeCurve != null)
                ratio = tween._easeCurve.Evaluate(ratio);
            else
                ratio = SEasing.Easing(tween._easeType, ratio);

            // easing power
            if (tween._easePow != 0)
            {
                ratio = Mathf.Pow(ratio, tween._easePow);
            }
        }

        // direction
        if (!tween._forward && tween._easeReverse)
            ratio = 1 - ratio;

        // update
#if STWEEN_DEBUG || UNITY_EDITOR
        try
        {
            tween._action.Update(ratio);
        }
        catch (Exception e)
        {
            Debug.LogError("STween Error : " + tween + "\r\n" + tween._action.Target, tween._action.Target as UnityEngine.Object);
            Debug.LogException(e);
        }
#else
        tween._action.Update(ratio);
#endif
        if (!tween.ExecuteOnUpdate())
        {
            tween.ExecuteOnComplete();
            Stop(tween);
            return true;
        }

        // end
        if (tweenEnd)
        {
            // check Loop for Remove Tween
            tweenEnd = false;
            switch (tween._loopType)
            {
                case SLoopType.Once:
                    tweenEnd = true;
                    break;

                case SLoopType.Clamp:
                    if (tween._loopCount == 0 || --tween.__procLoopCount > 0)
                    {
                        tween._startTime = targetTime;
                    }
                    else
                    {
                        tweenEnd = true;
                    }
                    break;

                case SLoopType.PingPong:
                    tween._forward = !tween._forward;
                    if (tween._forward == false || tween._loopCount == 0 || --tween.__procLoopCount > 0)
                    {
                        tween._startTime = targetTime;
                    }
                    else
                    {
                        tweenEnd = true;
                    }
                    break;
            }

            // remove Tween
            if (tweenEnd)
            {
                tween.ExecuteOnComplete();
                Stop(tween);
                return true;
            }
        }
        return false;
    }
}