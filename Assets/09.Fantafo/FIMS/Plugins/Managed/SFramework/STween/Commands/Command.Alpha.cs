﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SFramework.TweenAction;
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using UnityEngine.UI;
#endif

public partial class STween
{
    #region only To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Alpha                             //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState alpha(Transform trans, float to, float duration)
    {
        if (trans != null)
        {
            return alpha(trans.gameObject, to, duration);
        }
        return alpha((Material)null, to, duration);
    }
    public static STweenState alpha(GameObject go, float to, float duration)
    {
        if (go != null)
        {
            Renderer renderer;
            if ((renderer = go.GetComponent<Renderer>()) != null)
            {
                return alpha(renderer.material, to, duration);
            }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
            Graphic graphic;
            CanvasGroup canvasGroup;
            if ((graphic = go.GetComponent<Graphic>()) != null)
            {
                return alpha(graphic, to, duration);
            }
            else if ((canvasGroup = go.GetComponent<CanvasGroup>()) != null)
            {
                return alpha(canvasGroup, to, duration);
            }
#endif
        }
        return alpha((Material)null, to, duration);
    }

    public static STweenState alpha(Renderer renderer, float to, float duration)
    {
        return alpha(renderer != null ? renderer.material : null, to, duration);
    }
    public static STweenState alpha(Material material, float to, float duration)
    {
        return Play<ActionAlphaMaterial, Material, Color>(material, to, duration);
    }
    public static STweenState alpha(MeshFilter filter, float to, float duration)
    {
        return alpha((filter != null ? filter.mesh : null), to, duration);
    }
    public static STweenState alpha(Mesh mesh, float to, float duration)
    {
        return Play<ActionAlphaVertexMesh, Mesh, Color>(mesh, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState alpha(Graphic graphic, float to, float duration)
    {
        return Play<ActionAlphaGraphic, Graphic, Color>(graphic, to, duration);
    }
    public static STweenState alpha(CanvasGroup canvasGroup, float to, float duration)
    {
        return Play<ActionAlphaCanvasGroup, CanvasGroup, float>(canvasGroup, to, duration);
    }
#endif
    #endregion

    #region From, To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Alpha                             //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState alpha(Transform trans, float from, float to, float duration)
    {
        if (trans != null)
        {
            return alpha(trans.gameObject, from, to, duration);
        }
        return alpha((Material)null, from, to, duration);
    }
    public static STweenState alpha(GameObject go, float from, float to, float duration)
    {
        if (go != null)
        {
            Renderer renderer;
            if ((renderer = go.GetComponent<Renderer>()) != null)
            {
                return alpha(renderer.material, from, to, duration);
            }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
            Graphic graphic;
            CanvasGroup canvasGroup;
            if ((graphic = go.GetComponent<Graphic>()) != null)
            {
                return alpha(graphic, from, to, duration);
            }
            else if ((canvasGroup = go.GetComponent<CanvasGroup>()) != null)
            {
                return alpha(canvasGroup, from, to, duration);
            }
#endif
        }
        return alpha((Material)null, from, to, duration);
    }

    public static STweenState alpha(Renderer renderer, float from, float to, float duration)
    {
        return alpha(renderer != null ? renderer.material : null, from, to, duration);
    }
    public static STweenState alpha(Material material, float from, float to, float duration)
    {
        return Play<ActionAlphaMaterial, Material, Color>(material, from, to, duration);
    }
    public static STweenState alpha(MeshFilter filter, float from, float to, float duration)
    {
        return alpha((filter != null ? filter.mesh : null), from, to, duration);
    }
    public static STweenState alpha(Mesh mesh, float from, float to, float duration)
    {
        return Play<ActionAlphaVertexMesh, Mesh, Color>(mesh, from, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState alpha(Graphic graphic, float from, float to, float duration)
    {
        return Play<ActionAlphaGraphic, Graphic, Color>(graphic, from, to, duration);
    }
    public static STweenState alpha(CanvasGroup canvasGroup, float from, float to, float duration)
    {
        return Play<ActionAlphaCanvasGroup, CanvasGroup, float>(canvasGroup, from, to, duration);
    }
#endif
    #endregion
}
