﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    #region only To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Scale                            //
    //                                                             //
    /////////////////////////////////////////////////////////////////
    public static STweenState scale(Component comp, Vector3 to, float duration)
    {
        return scale(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState scale(GameObject go, Vector3 to, float duration)
    {
        return scale(go != null ? go.transform : null, to, duration);
    }
    public static STweenState scale(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionScale, Transform, Vector3>(trans, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Scale Single                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState scaleX(Component comp, float to, float duration)
    {
        return scaleX(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState scaleX(GameObject go, float to, float duration)
    {
        return scaleX(go != null ? go.transform : null, to, duration);
    }
    public static STweenState scaleX(Transform trans, float to, float duration)
    {
        return Play<ActionScaleX, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState scaleY(Component comp, float to, float duration)
    {
        return scaleY(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState scaleY(GameObject go, float to, float duration)
    {
        return scaleY(go != null ? go.transform : null, to, duration);
    }
    public static STweenState scaleY(Transform trans, float to, float duration)
    {
        return Play<ActionScaleY, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState scaleZ(Component comp, float to, float duration)
    {
        return scaleZ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState scaleZ(GameObject go, float to, float duration)
    {
        return scaleZ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState scaleZ(Transform trans, float to, float duration)
    {
        return Play<ActionScaleZ, Transform, Vector3>(trans, to, duration);
    }
    #endregion

    #region From, To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Scale                            //
    //                                                             //
    /////////////////////////////////////////////////////////////////
    public static STweenState scale(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return scale(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState scale(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return scale(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState scale(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionScale, Transform, Vector3>(trans, from, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Scale Single                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState scaleX(Component comp, float from, float to, float duration)
    {
        return scaleX(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState scaleX(GameObject go, float from, float to, float duration)
    {
        return scaleX(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState scaleX(Transform trans, float from, float to, float duration)
    {
        return Play<ActionScaleX, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState scaleY(Component comp, float from, float to, float duration)
    {
        return scaleY(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState scaleY(GameObject go, float from, float to, float duration)
    {
        return scaleY(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState scaleY(Transform trans, float from, float to, float duration)
    {
        return Play<ActionScaleY, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState scaleZ(Component comp, float from, float to, float duration)
    {
        return scaleZ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState scaleZ(GameObject go, float from, float to, float duration)
    {
        return scaleZ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState scaleZ(Transform trans, float from, float to, float duration)
    {
        return Play<ActionScaleZ, Transform, Vector3>(trans, from, to, duration);
    }
    #endregion
}
