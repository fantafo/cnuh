﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenButtonExtension
{
    private static readonly Action<Selectable, Color> _changeSelectableDisabledColor = __changeSelectableDisabledColor;
    private static void __changeSelectableDisabledColor(Selectable selectable, Color val) { var colors = selectable.colors; colors.disabledColor = val; selectable.colors = colors; }
    public static STweenState twnDisabledColor(this Selectable selectable, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, selectable.colors.disabledColor, to, duration, _changeSelectableDisabledColor);
    }
    public static STweenState twnDisabledColor(this Selectable selectable, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, from, to, duration, _changeSelectableDisabledColor);
    }


    private static readonly Action<Selectable, Color> _changeSelectableHighlightedColor = __changeSelectableHighlightedColor;
    private static void __changeSelectableHighlightedColor(Selectable selectable, Color val) { var colors = selectable.colors; colors.highlightedColor = val; selectable.colors = colors; }
    public static STweenState twnHighlightedColor(this Selectable selectable, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, selectable.colors.highlightedColor, to, duration, _changeSelectableHighlightedColor);
    }
    public static STweenState twnHighlightedColor(this Selectable selectable, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, from, to, duration, _changeSelectableHighlightedColor);
    }


    private static readonly Action<Selectable, Color> _changeSelectableNormalColor = __changeSelectableNormalColor;
    private static void __changeSelectableNormalColor(Selectable selectable, Color val) { var colors = selectable.colors; colors.normalColor = val; selectable.colors = colors; }
    public static STweenState twnNormalColor(this Selectable selectable, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, selectable.colors.normalColor, to, duration, _changeSelectableNormalColor);
    }
    public static STweenState twnNormalColor(this Selectable selectable, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, from, to, duration, _changeSelectableNormalColor);
    }


    private static readonly Action<Selectable, Color> _changeSelectablePressedColor = __changeSelectablePressedColor;
    private static void __changeSelectablePressedColor(Selectable selectable, Color val) { var colors = selectable.colors; colors.pressedColor = val; selectable.colors = colors; }
    public static STweenState twnPressedColor(this Selectable selectable, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, selectable.colors.pressedColor, to, duration, _changeSelectablePressedColor);
    }
    public static STweenState twnPressedColor(this Selectable selectable, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Selectable>, Selectable, Color>(selectable, from, to, duration, _changeSelectablePressedColor);
    }


    private static readonly Action<Selectable, float> _changeSelectableColorMultiplier = __changeSelectableColorMultiplier;
    private static void __changeSelectableColorMultiplier(Selectable selectable, float val) { var colors = selectable.colors; colors.colorMultiplier = val; selectable.colors = colors; }
    public static STweenState twnColorMultiplier(this Selectable selectable, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Selectable>, Selectable, float>(selectable, selectable.colors.colorMultiplier, to, duration, _changeSelectableColorMultiplier);
    }
    public static STweenState twnColorMultiplier(this Selectable selectable, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Selectable>, Selectable, float>(selectable, from, to, duration, _changeSelectableColorMultiplier);
    }


}
#endif