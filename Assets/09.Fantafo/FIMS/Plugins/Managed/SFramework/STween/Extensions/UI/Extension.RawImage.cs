﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenRawImageExtension
{
    private static readonly Action<RawImage, Rect> _changeRawImageUvRect = __changeRawImageUvRect;
    private static void __changeRawImageUvRect(RawImage rawImage, Rect val) { rawImage.uvRect = val; }
    public static STweenState twnUvRect(this RawImage rawImage, Rect to, float duration)
    {
        return STween.Play<ActionRectObject<RawImage>, RawImage, Rect>(rawImage, rawImage.uvRect, to, duration, _changeRawImageUvRect);
    }
    public static STweenState twnUvRect(this RawImage rawImage, Rect from, Rect to, float duration)
    {
        return STween.Play<ActionRectObject<RawImage>, RawImage, Rect>(rawImage, from, to, duration, _changeRawImageUvRect);
    }

}
#endif