﻿using UnityEngine;
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using UnityEngine.UI;
#endif

public static class STweenColorExtension
{
    #region only To
    public static STweenState twnColor(this Renderer renderer, Color to, float duration)
    {
        return STween.color(renderer, to, duration);
    }
    public static STweenState twnColor(this Material material, Color to, float duration)
    {
        return STween.color(material, to, duration);
    }
    public static STweenState twnColor(this MeshFilter filter, Color to, float duration)
    {
        return STween.color((filter != null ? filter.mesh : null), to, duration);
    }
    public static STweenState twnColor(this Mesh mesh, Color to, float duration)
    {
        return STween.color(mesh, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnColor(this Graphic graphic, Color to, float duration)
    {
        return STween.color(graphic, to, duration);
    }
#endif

    public static STweenState twnAlpha(this Renderer renderer, float to, float duration)
    {
        return STween.alpha(renderer, to, duration);
    }
    public static STweenState twnAlpha(this Material material, float to, float duration)
    {
        return STween.alpha(material, to, duration);
    }
    public static STweenState twnAlpha(this MeshFilter filter, float to, float duration)
    {
        return STween.alpha((filter != null ? filter.mesh : null), to, duration);
    }
    public static STweenState twnAlpha(this Mesh mesh, float to, float duration)
    {
        return STween.alpha(mesh, to, duration);
    }
    public static STweenState twnAlpha(this CanvasGroup group, float to, float duration)
    {
        return STween.alpha(group, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnAlpha(this Graphic graphic, float to, float duration)
    {
        return STween.alpha(graphic, to, duration);
    }
#endif
    #endregion

    #region From, To
    public static STweenState twnColor(this Renderer renderer, Color from, Color to, float duration)
    {
        return STween.color(renderer, from, to, duration);
    }
    public static STweenState twnColor(this Material material, Color from, Color to, float duration)
    {
        return STween.color(material, from, to, duration);
    }
    public static STweenState twnColor(this MeshFilter filter, Color from, Color to, float duration)
    {
        return STween.color((filter != null ? filter.mesh : null), from, to, duration);
    }
    public static STweenState twnColor(this Mesh mesh, Color from, Color to, float duration)
    {
        return STween.color(mesh, from, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnColor(this Graphic graphic, Color from, Color to, float duration)
    {
        return STween.color(graphic, from, to, duration);
    }
#endif

    public static STweenState twnAlpha(this Renderer renderer, float from, float to, float duration)
    {
        return STween.alpha(renderer, from, to, duration);
    }
    public static STweenState twnAlpha(this Material material, float from, float to, float duration)
    {
        return STween.alpha(material, from, to, duration);
    }
    public static STweenState twnAlpha(this MeshFilter filter, float from, float to, float duration)
    {
        return STween.alpha((filter != null ? filter.mesh : null), from, to, duration);
    }
    public static STweenState twnAlpha(this Mesh mesh, float from, float to, float duration)
    {
        return STween.alpha(mesh, from, to, duration);
    }
    public static STweenState twnAlpha(this CanvasGroup group, float from, float to, float duration)
    {
        return STween.alpha(group, from, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnAlpha(this Graphic graphic, float from, float to, float duration)
    {
        return STween.alpha(graphic, from, to, duration);
    }
#endif
    #endregion
}