﻿using UnityEngine;

public static class STweenComponentExtension
{
    #region only To
    public static STweenState twnMove(this Component comp, Vector3 to, float duration)
    {
        return STween.move(comp.transform, to, duration);
    }
    public static STweenState twnMoveLocal(this Component comp, Vector3 to, float duration)
    {
        return STween.moveLocal(comp.transform, to, duration);
    }
    public static STweenState twnMoveX(this Component comp, float to, float duration)
    {
        return STween.moveX(comp.transform, to, duration);
    }
    public static STweenState twnMoveY(this Component comp, float to, float duration)
    {
        return STween.moveY(comp.transform, to, duration);
    }
    public static STweenState twnMoveZ(this Component comp, float to, float duration)
    {
        return STween.moveZ(comp.transform, to, duration);
    }
    public static STweenState twnMoveLocalX(this Component comp, float to, float duration)
    {
        return STween.moveLocalX(comp.transform, to, duration);
    }
    public static STweenState twnMoveLocalY(this Component comp, float to, float duration)
    {
        return STween.moveLocalY(comp.transform, to, duration);
    }
    public static STweenState twnMoveLocalZ(this Component comp, float to, float duration)
    {
        return STween.moveLocalZ(comp.transform, to, duration);
    }

    public static STweenState twnRotate(this Component comp, Vector3 to, float duration)
    {
        return STween.rotate(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocal(this Component comp, Vector3 to, float duration)
    {
        return STween.rotateLocal(comp.transform, to, duration);
    }
    public static STweenState twnRotate(this Component comp, Quaternion to, float duration)
    {
        return STween.rotate(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocal(this Component comp, Quaternion to, float duration)
    {
        return STween.rotateLocal(comp.transform, to, duration);
    }
    public static STweenState twnRotateQ(this Component comp, Vector3 to, float duration)
    {
        return STween.rotateQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Component comp, Vector3 to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateQ(this Component comp, Quaternion to, float duration)
    {
        return STween.rotateQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Component comp, Quaternion to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, to, duration);
    }


    public static STweenState twnRotateX(this Component comp, float to, float duration)
    {
        return STween.rotateX(comp.transform, to, duration);
    }
    public static STweenState twnRotateY(this Component comp, float to, float duration)
    {
        return STween.rotateY(comp.transform, to, duration);
    }
    public static STweenState twnRotateZ(this Component comp, float to, float duration)
    {
        return STween.rotateZ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalX(this Component comp, float to, float duration)
    {
        return STween.rotateLocalX(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalY(this Component comp, float to, float duration)
    {
        return STween.rotateLocalY(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalZ(this Component comp, float to, float duration)
    {
        return STween.rotateLocalZ(comp.transform, to, duration);
    }

    public static STweenState twnScale(this Component comp, Vector3 to, float duration)
    {
        return STween.scale(comp.transform, to, duration);
    }
    public static STweenState twnScaleX(this Component comp, float to, float duration)
    {
        return STween.scaleX(comp.transform, to, duration);
    }
    public static STweenState twnScaleY(this Component comp, float to, float duration)
    {
        return STween.scaleY(comp.transform, to, duration);
    }
    public static STweenState twnScaleZ(this Component comp, float to, float duration)
    {
        return STween.scaleZ(comp.transform, to, duration);
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnSize(this Component comp, Vector2 to, float duration)
    {
        return STween.size(comp.transform as RectTransform, to, duration);
    }
    public static STweenState twnSizeX(this Component comp, float to, float duration)
    {
        return STween.sizeX(comp.transform as RectTransform, to, duration);
    }
    public static STweenState twnSizeY(this Component comp, float to, float duration)
    {
        return STween.sizeY(comp.transform as RectTransform, to, duration);
    }

    public static STweenState twnAnchored(this Component comp, Vector3 to, float duration)
    {
        return STween.anchored(comp.transform as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredX(this Component comp, float to, float duration)
    {
        return STween.anchoredX(comp.transform as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredY(this Component comp, float to, float duration)
    {
        return STween.anchoredY(comp.transform as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredZ(this Component comp, float to, float duration)
    {
        return STween.anchoredZ(comp.transform as RectTransform, to, duration);
    }
#endif

    public static STweenState twnAlpha(this Component comp, float to, float duration)
    {
        return STween.alpha(comp.transform, to, duration);
    }
    public static STweenState twnColor(this Component comp, Color to, float duration)
    {
        return STween.color(comp.transform, to, duration);
    }
    #endregion

    #region From, To
    public static STweenState twnMove(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.move(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveLocal(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.moveLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveX(this Component comp, float from, float to, float duration)
    {
        return STween.moveX(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveY(this Component comp, float from, float to, float duration)
    {
        return STween.moveY(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveZ(this Component comp, float from, float to, float duration)
    {
        return STween.moveZ(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveLocalX(this Component comp, float from, float to, float duration)
    {
        return STween.moveLocalX(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveLocalY(this Component comp, float from, float to, float duration)
    {
        return STween.moveLocalY(comp.transform, from, to, duration);
    }
    public static STweenState twnMoveLocalZ(this Component comp, float from, float to, float duration)
    {
        return STween.moveLocalZ(comp.transform, from, to, duration);
    }

    public static STweenState twnRotate(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotate(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocal(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnRotate(this Component comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotate(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocal(this Component comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateQ(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateQ(this Component comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalQ(this Component comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, from, to, duration);
    }


    public static STweenState twnRotateX(this Component comp, float from, float to, float duration)
    {
        return STween.rotateX(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateY(this Component comp, float from, float to, float duration)
    {
        return STween.rotateY(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateZ(this Component comp, float from, float to, float duration)
    {
        return STween.rotateZ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalX(this Component comp, float from, float to, float duration)
    {
        return STween.rotateLocalX(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalY(this Component comp, float from, float to, float duration)
    {
        return STween.rotateLocalY(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalZ(this Component comp, float from, float to, float duration)
    {
        return STween.rotateLocalZ(comp.transform, from, to, duration);
    }

    public static STweenState twnScale(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.scale(comp.transform, from, to, duration);
    }

    public static STweenState twnScaleX(this Component comp, float from, float to, float duration)
    {
        return STween.scaleX(comp.transform, from, to, duration);
    }
    public static STweenState twnScaleY(this Component comp, float from, float to, float duration)
    {
        return STween.scaleY(comp.transform, from, to, duration);
    }
    public static STweenState twnScaleZ(this Component comp, float from, float to, float duration)
    {
        return STween.scaleZ(comp.transform, from, to, duration);
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnSize(this Component comp, Vector2 from, Vector2 to, float duration)
    {
        return STween.size(comp.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnSizeX(this Component comp, float from, float to, float duration)
    {
        return STween.sizeX(comp.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnSizeY(this Component comp, float from, float to, float duration)
    {
        return STween.sizeY(comp.transform as RectTransform, from, to, duration);
    }

    public static STweenState twnAnchored(this Component comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.anchored(comp.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredX(this Component comp, float from, float to, float duration)
    {
        return STween.anchoredX(comp.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredY(this Component comp, float from, float to, float duration)
    {
        return STween.anchoredY(comp.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredZ(this Component comp, float from, float to, float duration)
    {
        return STween.anchoredZ(comp.transform as RectTransform, from, to, duration);
    }
#endif

    public static STweenState twnAlpha(this Component comp, float from, float to, float duration)
    {
        return STween.alpha(comp.transform, from, to, duration);
    }
    public static STweenState twnColor(this Component comp, Color from, Color to, float duration)
    {
        return STween.color(comp.transform, from, to, duration);
    }
    #endregion
}