﻿using UnityEngine;

namespace SFramework
{
    public static class STime
    {
        public static float unscaledTime
        {
            get
            {
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3
                return Time.time / Time.timeScale;
#else
                return Time.unscaledTime;
#endif
            }
        }
    }
}
