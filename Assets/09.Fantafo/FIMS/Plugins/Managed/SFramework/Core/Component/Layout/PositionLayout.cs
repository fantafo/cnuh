﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace SFramework
{
    [Flags]
    public enum PositionLayoutType : byte
    {
        None = 0,
        X = 1,
        Y = 2,
        Z = 4,
    }

    [Flags]
    public enum PositionLayoutAlignment : byte
    {
        None = 0,
        Front = 0,
        Inner = (1 << 0),
        Back = (1 << 1),

        Left = 0,
        Center = (1 << 2),
        Right = (1 << 3),

        Top = 0,
        Middle = (1 << 4),
        Bottom = (1 << 5),

        Front_Left_Top = Left | Front | Top,
        Front_Cetner_Middle = Center | Front | Middle,
        Front_Cetner_Bottom = Center | Front | Bottom,
        Inner_Center_Middle = Inner | Center | Middle,
        Front_Center_Top = Front | Center | Top,

        MASK_X = Left | Center | Right,
        MASK_Y = Top | Middle | Bottom,
        MASK_Z = Front | Inner | Back,
    }

    [ExecuteInEditMode]
    [AddComponentMenu("SF/Layout/Position Layout")]
    public class PositionLayout : MonoBehaviour
    {
        int _x, _y, _z;
        Transform _trans;

#if UNITY_EDITOR
        public bool executeInEditMode;
#endif
        public bool useLocal;
        public bool useRelativeAlignment;
        public bool useOnlyActive;
        public Vector3 spacing;

        public bool firstDotSorting = false;
        public PositionLayoutType firstLayout = PositionLayoutType.X;
        public PositionLayoutType secondLayout;
        public PositionLayoutType thirdlLayout;
        public PositionLayoutAlignment alignment = PositionLayoutAlignment.Front_Center_Top;

        public int firstCount;
        public int secondCount;

        List<Transform> transforms;


        void Awake()
        {
            _trans = transform;
        }

#if UNITY_EDITOR
        public void OnTransformChildrenChanged()
        {
            if (enabled && executeInEditMode && !Application.isPlaying)
                Reposition();
        }

        public void OnValidate()
        {
            if (enabled && executeInEditMode && !Application.isPlaying)
                Reposition();
        }
#endif

        int ChangeAlignmentMask(PositionLayoutAlignment flag, PositionLayoutAlignment mask, PositionLayoutAlignment center)
        {
            //var a = flag & mask;
            //var b = (int)a;
            //var c = BitHelper.IndexOf((int)center);
            //var r = b >> c;
            //return r;
            return (int)(flag & mask) >> BitHelper.IndexOf((int)center);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (UnityEditor.EditorApplication.isCompiling)
                return;
            if (executeInEditMode)
                Reposition();
#endif
        }

        [ContextMenu("Reposition")]
        public void Reposition()
        {
            if (_trans == null)
                _trans = transform;

            if (transforms == null)
                transforms = new List<Transform>();
            else
                transforms.Clear();

            for(int i=0, len = _trans.childCount; i<len; i++)
            {
                Transform c = _trans.GetChild(i);
                if(!useOnlyActive || c.gameObject.activeSelf)
                {
                    transforms.Add(c);
                }
            }

            _x = 0;
            _y = 0;
            _z = 0;

            // variable
            Transform child;
            Vector3 temp;

            // my
            Vector3 anchor;
            if (useLocal)
                anchor = Vector3.zero;
            else
                anchor = _trans.position;

            if (alignment != PositionLayoutAlignment.None)
            {
                // calc size
                Vector3 alignPos = GetCounts();
                alignPos -= Vector3.one;
                alignPos = alignPos.Multiply(spacing);

                // calc space
                alignPos.x *= (ChangeAlignmentMask(alignment, PositionLayoutAlignment.MASK_X, PositionLayoutAlignment.Center) * 0.5f);
                alignPos.y *= (ChangeAlignmentMask(alignment, PositionLayoutAlignment.MASK_Y, PositionLayoutAlignment.Middle) * 0.5f);
                alignPos.z *= (ChangeAlignmentMask(alignment, PositionLayoutAlignment.MASK_Z, PositionLayoutAlignment.Inner) * 0.5f);

                anchor += alignPos;
            }

            int[] sortingMethod = null;
            if (firstDotSorting)
            {
                int totalLength = firstCount;
                int maxValue = (firstCount - 1);
                bool odd = ((totalLength % 2) == 1); // 홀수
                if (odd) 
                    maxValue--;

                sortingMethod = new int[totalLength];
                for (int i = 0; i <= maxValue; i++)
                {
                    int index = Mathf.Abs(maxValue - i * 2) - Mathf.Clamp(i - (maxValue / 2), 0, 1);
                    sortingMethod[index] = i;
                }

                if (odd)
                    sortingMethod[totalLength - 1] = totalLength - 1;
            }

            // layout
            for (int i = 0; i < transforms.Count; i++)
            {
                child = transforms[i];

                if (firstCount != 0 && secondLayout != PositionLayoutType.None)
                {
                    if (firstDotSorting)
                    {
                        FirstValue = sortingMethod[i % firstCount];
                    }
                    else
                    {
                        FirstValue = i % firstCount;
                    }
                    SecondValue = (i / firstCount);
                    if (thirdlLayout != PositionLayoutType.None && secondCount != 0)
                    {
                        SecondValue %= secondCount;
                        ThirdValue = i / (firstCount * secondCount);
                    }
                }
                else
                {
                    FirstValue = i;
                }

                temp = anchor + new Vector3(-spacing.x * _x, -spacing.y * _y, -spacing.z * _z);
                if (useLocal)
                {
                    child.localPosition = temp;
                }
                else
                {
                    child.position = temp;
                }
            }
        }


        int FirstValue
        {
            get
            {
                switch (firstLayout)
                {
                    case PositionLayoutType.X:
                        return _x;
                    case PositionLayoutType.Y:
                        return _y;
                    case PositionLayoutType.Z:
                        return _z;
                }
                return 0;
            }
            set
            {
                switch (firstLayout)
                {
                    case PositionLayoutType.X:
                        _x = value;
                        break;
                    case PositionLayoutType.Y:
                        _y = value;
                        break;
                    case PositionLayoutType.Z:
                        _z = value;
                        break;
                }
            }
        }
        int SecondValue
        {
            get
            {
                switch (secondLayout)
                {
                    case PositionLayoutType.X:
                        return _x;
                    case PositionLayoutType.Y:
                        return _y;
                    case PositionLayoutType.Z:
                        return _z;
                }
                return 0;
            }
            set
            {
                switch (secondLayout)
                {
                    case PositionLayoutType.X:
                        _x = value;
                        break;
                    case PositionLayoutType.Y:
                        _y = value;
                        break;
                    case PositionLayoutType.Z:
                        _z = value;
                        break;
                }
            }
        }
        int ThirdValue
        {
            get
            {
                switch (thirdlLayout)
                {
                    case PositionLayoutType.X:
                        return _x;
                    case PositionLayoutType.Y:
                        return _y;
                    case PositionLayoutType.Z:
                        return _z;
                }
                return 0;
            }
            set
            {
                switch (thirdlLayout)
                {
                    case PositionLayoutType.X:
                        _x = value;
                        break;
                    case PositionLayoutType.Y:
                        _y = value;
                        break;
                    case PositionLayoutType.Z:
                        _z = value;
                        break;
                }
            }
        }


        Vector3 GetCounts()
        {
            return new Vector3(GetCount(PositionLayoutType.X), GetCount(PositionLayoutType.Y), GetCount(PositionLayoutType.Z));
        }
        int GetCount(PositionLayoutType type)
        {
            if (firstLayout == type)
            {
                if(useRelativeAlignment)
                {
                    int childcount = transforms.Count;
                    if(firstCount <= 0 || childcount < firstCount || secondLayout == PositionLayoutType.None)
                    {
                        return childcount;
                    }
                    else
                    {
                        return firstCount;
                    }
                }
                else
                {
                    return firstCount;
                }
            }
            else if (secondLayout == type && firstCount != 0)
            {
                if(useRelativeAlignment)
                {
                    int childcount = transforms.Count;
                    if (secondCount <= 0 || childcount < secondCount || thirdlLayout == PositionLayoutType.None)
                    {
                        return childcount;
                    }
                    else
                    {
                        return secondCount;
                    }
                }
                else
                {
                    return secondCount;
                }
            }
            else if (thirdlLayout == type && firstCount != 0 && secondCount != 0)
            {
                return (transforms.Count / (firstCount * secondCount)) + 1;
            }
            return 1;
        }
    }
}