﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace SFramework
{
    public abstract class ObjectPool<T> : IDisposable
    {

        private Stack<T> stack;

        public bool useWarning = true;
        public int initializeCount = 10;
        public int increaseCount = 5;
        public int maxCount = int.MaxValue;

        protected abstract T OnCreate();
        protected virtual void OnTake(T obj) { }
        protected virtual void OnRelease(T obj) { }
        protected virtual void OnDestroy(T obj) { }
        protected virtual void OnDispose() { }



        /// <summary>
        /// prepare pools
        /// </summary>
        public void Prepare()
        {
            if (stack != null)
                return;

            stack = new Stack<T>(initializeCount);
            for (int i = 0; i < initializeCount; i++)
            {
                stack.Push(OnCreate());
            }
        }

        /// <summary>
        /// increase pool
        /// </summary>
        public void Push(int count)
        {
            for (int i = 0; i < count; i++)
            {
                stack.Push(OnCreate());
            }
        }

        /// <summary>
        /// take object
        /// </summary>
        public T Take()
        {
            Prepare();

            T obj;
            if (stack.Count == 0)
            {
                if (useWarning)
                    Debug.LogWarning("Object Pool Over Count. Upgrade InitializeCount. " + GetType().Name + "<" + typeof(T).Name + ">");

                for (int i = 1; i < increaseCount; i++)
                {
                    stack.Push(OnCreate());
                }
                obj = OnCreate();
            }
            else
            {
                obj = stack.Pop();
            }

            OnTake(obj);
            return obj;
        }

        /// <summary>
        /// release object
        /// </summary>
        public void Release(T obj)
        {
            if (stack.Count > maxCount)
            {
                OnDestroy(obj);
                return;
            }

            OnRelease(obj);
            stack.Push(obj);
        }

        /// <summary>
        /// dispose Pool
        /// </summary>
        public void Dispose()
        {
            for (int i = 0, len = stack.Count; i < len; i++)
            {
                OnDestroy(stack.Pop());
            }

            OnDispose();

            stack = null;
        }
    }
}